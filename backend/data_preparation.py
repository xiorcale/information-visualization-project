import json
import os
import redis
from backend.parser import parse_dataset, remove_data_outside_of_canton
from backend.yelp import retrieve
from backend.config import CANTONS, STATES

def insert_cantons_into_db(db):
    ratings, prices = parse_dataset()

    db.set('ratings', str(ratings))
    db.set('prices', str(prices))


def insert_businesses_into_db(db):
    files_list = [
        f'dataset/{canton}.json'
        for canton in CANTONS
    ]

    for i, filename in enumerate(files_list):
        try:
            with open(filename, 'r') as canton:
                dataset = json.load(canton)
                dataset = remove_data_outside_of_canton(dataset, STATES[i])
                db.set(CANTONS[i], str(dataset))
        except FileNotFoundError:
            print(f"File {filename} not found. If you want to refresh all data, delete the dataset directory.")
        

def create_dataset_if_not_exist(db):
    if not os.path.isdir("dataset"):
        os.mkdir('dataset')
        print('Start retrieving cantons')
        retrieve()
        print('Retrived all cantons')
        insert_businesses_into_db(db)
        insert_cantons_into_db(db)